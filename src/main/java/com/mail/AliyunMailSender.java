package com.mail;

import com.jfinal.log.Log;
import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 * Created by zhuge on 2017/11/28.
 */
public abstract class AliyunMailSender {

    public static final Log log = Log.getLog(AliyunMailSender.class);
    private  static String fromAddress = "zhuliangcai@aliyun.com";
    private  static String mailHost = "smtp.aliyun.com";

    public static void sendMail(String subject, String mailText, String... toAddresses) {

        try {

            Address[] addresses = new Address[toAddresses.length];
            for (int i = 0; i < toAddresses.length; i++) {
                addresses[i] = new InternetAddress(toAddresses[i]);
            }

            Properties props = new Properties();

            // 开启debug调试
            props.setProperty("mail.debug", "true");
            // 发送服务器需要身份验证
            props.setProperty("mail.smtp.auth", "true");
            // 设置邮件服务器主机名
            props.setProperty("mail.host",mailHost);
            // 发送邮件协议名称
            props.setProperty("mail.transport.protocol", "smtp");

            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            props.put("mail.smtp.ssl.enable", "true");
            props.put("mail.smtp.ssl.socketFactory", sf);

            Session session = Session.getInstance(props);

            Message msg = new MimeMessage(session);
            msg.setSubject(subject);
            StringBuilder builder = new StringBuilder();
            builder.append(mailText);
            builder.append("\n时间 " + new Date());
            msg.setText(builder.toString());
            //发送方
            msg.setFrom(new InternetAddress(fromAddress));

            Transport transport = session.getTransport();
            transport.connect(mailHost, fromAddress,
                    "1024955966zlc");


            transport.sendMessage(msg, addresses);
            transport.close();


        } catch (Exception e) {
            log.error("AliyunMailSender邮件发送程序异常"+
                    "邮件发送程序异常"+"zhuliangcai@itcast.cn");


        }

    }
}
