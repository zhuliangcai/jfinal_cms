package com.jflyfox.modules.admin.advertisement;

import com.jfinal.log.Log;
import com.jflyfox.system.dict.SysDictDetail;
import com.jflyfox.util.cache.Cache;
import com.jflyfox.util.cache.CacheManager;

import java.util.ArrayList;
import java.util.List;

public class AdvertisementCache {
    private final static Log log = Log.getLog(AdvertisementCache.class);
    private final static String cacheName = "AdvertisementCache";
    private static Cache cache;
    public static void init() {
        log.info("####AdvertisementCache......");
        if (cache == null) {
            cache = CacheManager.get(cacheName);
        }
        update();

    }

    /**
     * 更新友情链接缓存
     *
     * 2015年4月24日 下午3:11:40 flyfox 369191470@qq.com
     */
    public static void update() {
        cache.clear();
        List<SysDictDetail> typeList = SysDictDetail.dao.findByWhere(" where dict_type = 'friendlyLinkType'");
        for (SysDictDetail dict : typeList) {
            int type = dict.getInt("detail_id");
            List<Advertisement> list = Advertisement.dao.findByWhere(" where type = ? order by sort ", type);
            cache.add(type + "", list);
        }

    }

    public static List<Advertisement> getList(int type) {
        return cache.get(type + "");
    }
    /**
     * 广告
     *
     * 2016年5月4日 下午2:50:27
     * zhuge 1024955966@qq.com
     * @param siteId
     * @return
     */
    public static List<Advertisement> getAdvertisementList(int siteId) {
        List<Advertisement> list = cache.get(108 + "");
        List<Advertisement> newList = new ArrayList<>();

        for(Advertisement item : list){
            Integer tmpSiteId = item.getInt("site_id");
            if (tmpSiteId <= 0 || tmpSiteId == siteId ) {
                if(item.getState()==1) {  // 有效的广告 state=1
                    newList.add(item);
                }
            }
        }

        return newList;
    }


}
