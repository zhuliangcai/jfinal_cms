package com.jflyfox.modules.admin.advertisement;

import com.jflyfox.component.base.BaseProjectModel;
import com.jflyfox.jfinal.component.annotation.ModelBind;

/**
 * CREATE TABLE `tb_advertisement` (
 `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
 `name` VARCHAR(256) NOT NULL COMMENT '广告名称/11111/',
 `url` VARCHAR(256) NOT NULL COMMENT 'URL',
 `content` VARCHAR(2000) NOT NULL COMMENT '广告主体-必填',
 `state` INT(11) DEFAULT '0' COMMENT '是否显示//radio/1,显示,2,不显示',
 `sort` INT(11) DEFAULT '21' COMMENT '排序',
 `type` INT(11) DEFAULT '21' COMMENT '类型//select/1,见数据字典',
 `remark` VARCHAR(256) DEFAULT NULL COMMENT '备注//textarea',
 `site_id` INT(11) DEFAULT '0' COMMENT '站点ID',
 `create_time` VARCHAR(64) DEFAULT NULL COMMENT '创建时间',
 `update_time` VARCHAR(64) DEFAULT SYSDATE() COMMENT '更新时间',
 `create_id` INT(11) DEFAULT '0' COMMENT '创建者',
 PRIMARY KEY (`id`)
 ) ENGINE=INNODB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='广告表'
 */
@ModelBind(table = "tb_advertisement")
public class Advertisement extends BaseProjectModel<Advertisement> {

	private static final long serialVersionUID = 1L;
	public static final Advertisement dao = new Advertisement();

	// columns START
	private String ID = "id"; // 主键
	private String NAME = "name"; // 名称/11111/
	private String URL = "url"; // URL
	private String CONTENT = "content"; // content
	private String STATE = "state"; // 是否显示//radio/1,显示,2,不显示
	private String SORT = "sort"; // 排序号
	private String TYPE = "type"; // 类型//select/1,见数据字典
	private String REMARK = "remark"; // 备注//textarea
	private String CREATE_TIME = "create_time"; // 创建时间
	private String UPDATE_TIME = "update_time"; // 更新时间
	private String CREATE_ID = "create_id"; // 创建者

	public Advertisement setId(Integer value) {
		set(ID, value);
		return this;
	}

	public Integer getId() {
		return get(ID);
	}

	public Advertisement setName(String value) {
		set(NAME, value);
		return this;
	}

	public String getName() {
		return get(NAME);
	}


	public Advertisement setContent(String value) {
		set(CONTENT, value);
		return this;
	}

	public String getContent() {
		return get(CONTENT);
	}

	public Advertisement setUrl(String value) {
		set(URL, value);
		return this;
	}

	public String getUrl() {
		return get(URL);
	}

	public Advertisement setSort(Integer value) {
		set(SORT, value);
		return this;
	}

	public Integer getSort() {
		return get(SORT);
	}

	public Advertisement setState(Integer value) {
		set(STATE, value);
		return this;
	}

	public Integer getState() {
		return get(STATE);
	}

	public Advertisement setType(Integer value) {
		set(TYPE, value);
		return this;
	}

	public Integer getType() {
		return get(TYPE);
	}

	public Advertisement setRemark(String value) {
		set(REMARK, value);
		return this;
	}

	public String getRemark() {
		return get(REMARK);
	}

	public Advertisement setCreateTime(String value) {
		set(CREATE_TIME, value);
		return this;
	}

	public String getCreateTime() {
		return get(CREATE_TIME);
	}
	public String getUpateTime() {
		return get(UPDATE_TIME);
	}

	public Advertisement setUpateTime(String value) {
		set(UPDATE_TIME, value);
		return this;
	}
	public Advertisement setCreateId(Integer value) {
		set(CREATE_ID, value);
		return this;
	}

	public Integer getCreateId() {
		return get(CREATE_ID);
	}
}